from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from config import DevelopmentConfig, TestingConfig
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_migrate import Migrate
import os
from flask_jwt_extended import JWTManager
from flask_uploads import UploadSet, IMAGES, configure_uploads

db = SQLAlchemy()
mail = Mail()
bcrypt = Bcrypt()
cors = CORS()
migrate = Migrate()
jwt = JWTManager()

photos = UploadSet('photos', IMAGES)


def create_app(testing=False):
    app = Flask(__name__, root_path=os.getcwd())
    if not testing:
        app.config.from_object(DevelopmentConfig)
    else:
        app.config.from_object(TestingConfig)
    app.config['JWT_SECRET_KEY'] = os.urandom(35)
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access']

    db.init_app(app)
    mail.init_app(app)
    cors.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)

    from app.infrastructure.models import role, product, file_uploads, order, \
        user, car, comments, categories, \
        message, wishlist, addresse, quote

    from app.application.blueprint.product import products
    from app.application.blueprint.auth import users

    with app.app_context():
        app.register_blueprint(users)
        app.register_blueprint(products)

        configure_uploads(app, (photos,))

    return app
