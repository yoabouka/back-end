# Roadmap

## Order management

- Saving orders sent by external client
- Cancel orders sent by external client
- Get all orders of an external client
- Get all orders of an external seller
- Get order details from a client
- Get order details from a seller
- Handle orders (seller level task)

## Product management

- Listing products
- Retrieving a single product details
- Editing a product
- Adding a new product
- Deleting a product
- Listing products of a specific seller
- Retrieving products by category
- Search products(filter by different ways such as : categories, name, reference if is a part, by carBrand and carModel)
- comment product
- rate product(noter un produit)

## Category management

- Listing categories
- Add a new category


## Comment management
- add comment on specific product by specific user
- delete comment
- rate product(rating of product add star or deprecated)
- edit comment
- hide comment(admin only)
- signal comment

## quote management
 - add quote(to send quote demand to seller)
 - list all user quote
 - delete quote
 - edit quote
 
 
## wishlist management
///
## paiement
//
## message
//
## admin task
//